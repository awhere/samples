﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace aWhere.Api.Modeling.Sample {

    internal class Program {

        #region Constants

        public const string CROP_MODELS_URL = "https://data.awhere.com/api/models/crop";

        public const string USERNAME = "username";
        public const string PASSWORD = "password";

        #endregion Constants

        #region Methods

        private static void Main(string[] args) {
            Console.WriteLine("Please select an option:");
            Console.WriteLine();
            Console.WriteLine("1. Make a batch request via POST");
            Console.WriteLine("2. Make a single request via GET");
            Console.WriteLine("3. Make a single request via GET with the model name specified");

            string jsonResponse = string.Empty;

            ConsoleKeyInfo keyInfo = Console.ReadKey();

            if (keyInfo.Key == ConsoleKey.D1 || keyInfo.Key == ConsoleKey.NumPad1) {
                List<CropModelsRequest> requests = BuildExampleCropModelsRequestsForPost();
                jsonResponse = PostCropModelsRequestJson(requests, string.Empty).Result;
                Console.WriteLine(jsonResponse);
            } else if (keyInfo.Key == ConsoleKey.D2 || keyInfo.Key == ConsoleKey.NumPad2) {
                CropModelsRequest requests = BuildExampleCropModelsRequestsForGet(false);
                jsonResponse = GetCropModelsJson(requests).Result;
                Console.WriteLine(jsonResponse);
            } else if (keyInfo.Key == ConsoleKey.D3 || keyInfo.Key == ConsoleKey.NumPad3) {
                CropModelsRequest requests = BuildExampleCropModelsRequestsForGet(true);
                jsonResponse = GetCropModelsJson(requests).Result;
                Console.WriteLine(jsonResponse);
            }

            Console.WriteLine();
            Console.WriteLine("Press any key to Exit...");
            Console.ReadKey();
        }

        /// <summary>
        /// Sample batch POST request to the Crop Models API, using the Web API 2.1 client library
        /// </summary>
        /// <returns>Task resulting in the Json response</returns>
        private static async Task<string> PostCropModelsRequestJson(List<CropModelsRequest> batch, string modelType) {
            string jsonResponses = string.Empty;

            using (var httpHandler = new HttpClientHandler()) {
                using (var httpClient = new HttpClient(httpHandler)) {
                    httpClient.Timeout = new TimeSpan(0, 10, 0);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", EncodeCredentials(USERNAME, PASSWORD));

                    string url = !string.IsNullOrWhiteSpace(modelType) ? CROP_MODELS_URL + @"/crop/" + modelType : CROP_MODELS_URL + @"/crop/";

                    HttpResponseMessage response = await httpClient.PostAsJsonAsync(url, batch);
                    if (response.IsSuccessStatusCode) {
                        jsonResponses = await response.Content.ReadAsStringAsync();
                    }
                }
            }

            return jsonResponses;
        }

        /// <summary>
        /// Sample batch POST request to the Crop Models API, using the Web API 2.1 client library
        /// </summary>
        /// <returns>Task resulting in a List of CropModelResponse objects</returns>
        private static async Task<List<CropModelsResponse>> PostCropModelsRequest(List<CropModelsRequest> batch, string modelType) {
            List<CropModelsResponse> responses = new List<CropModelsResponse>();

            using (var httpHandler = new HttpClientHandler()) {
                using (var httpClient = new HttpClient(httpHandler)) {
                    httpClient.Timeout = new TimeSpan(0, 10, 0);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", EncodeCredentials(USERNAME, PASSWORD));

                    string url = !string.IsNullOrWhiteSpace(modelType) ? CROP_MODELS_URL + @"/crop/" + modelType : CROP_MODELS_URL + @"/crop/";

                    HttpResponseMessage response = await httpClient.PostAsJsonAsync(url, batch);
                    if (response.IsSuccessStatusCode) {
                        responses = await response.Content.ReadAsAsync<List<CropModelsResponse>>();
                    }
                }
            }

            return responses;
        }

        /// <summary>
        /// Sample GET request to the Models API, using the Web API 2.1 client library
        /// </summary>
        /// <returns>Task resulting in a CropModelResponse object</returns>
        private static async Task<CropModelsResponse> GetCropModels(CropModelsRequest request) {
            CropModelsResponse modelResponse = null;

            using (var httpHandler = new HttpClientHandler()) {
                using (var httpClient = new HttpClient(httpHandler)) {
                    httpClient.Timeout = new TimeSpan(0, 10, 0);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BASIC", EncodeCredentials(USERNAME, PASSWORD));

                    string url = BuildUrlWithExampleQueryString(request);
                    HttpResponseMessage response = await httpClient.GetAsync(url);
                    if (response.IsSuccessStatusCode) {
                        return await response.Content.ReadAsAsync<CropModelsResponse>();
                    }
                }
            }

            return modelResponse;
        }

        /// <summary>
        /// Sample GET request to the Weather API, using the Web API 2.1 client library
        /// </summary>
        /// <returns>Task resulting in the Json response</returns>
        private static async Task<string> GetCropModelsJson(CropModelsRequest request) {
            string modelResponse = null;

            using (var httpHandler = new HttpClientHandler()) {
                using (var httpClient = new HttpClient(httpHandler)) {
                    httpClient.Timeout = new TimeSpan(0, 10, 0);
                    httpClient.DefaultRequestHeaders.Accept.Clear();
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("BASIC", EncodeCredentials(USERNAME, PASSWORD));

                    string url = BuildUrlWithExampleQueryString(request);
                    HttpResponseMessage response = await httpClient.GetAsync(url);
                    if (response.IsSuccessStatusCode) {
                        return await response.Content.ReadAsStringAsync();
                    }
                }
            }

            return modelResponse;
        }

        /// <summary>
        /// Formats and encodes credentials appropriately for Basic Authentication
        /// </summary>
        private static string EncodeCredentials(string username, string password) {
            string encodedCredentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(username + ":" + password));
            return encodedCredentials;
        }

        /// <summary>
        /// Builds requests for demonstration of POST calls.
        /// </summary>
        private static List<CropModelsRequest> BuildExampleCropModelsRequestsForPost() {
            List<CropModelsRequest> cropModelsRequests = new List<CropModelsRequest>();

            DateTime tomorrow = DateTime.Today.AddDays(1);

            cropModelsRequests.Add(new CropModelsRequest()
            {
                Latitude = 41.672,
                Longitude = -91.347,
                Date = tomorrow,
                ModelName = ModelTypes.CornRust.ToString(),
                UtcOffset = new TimeSpan(-6, 0, 0)
            });
            cropModelsRequests.Add(new CropModelsRequest()
            {
                Latitude = 59.213,
                Longitude = 25.621,
                Date = tomorrow,
                MinTemperature = 10,
                MaxTemperature = 30,
                OptimumTemperature = 20,
                MinWetHours = 4,
                MaxDryHours = 1,
                UtcOffset = new TimeSpan(2, 0, 0)
            });

            return cropModelsRequests;
        }

        /// <summary>
        /// Builds a request for demonstration of Get calls.
        /// </summary>
        /// <param name="isModelNameSpecified">Indicates whether a model type was specified by name in the request</param>
        /// <returns></returns>
        private static CropModelsRequest BuildExampleCropModelsRequestsForGet(bool isModelNameSpecified) {
            DateTime tomorrow = DateTime.Today.AddDays(1);

            if (isModelNameSpecified) {
                return new CropModelsRequest()
                {
                    ModelName = ModelTypes.CornRust.ToString(),
                    Latitude = 10.477,
                    Longitude = -69.296,
                    Date = tomorrow,
                    UtcOffset = new TimeSpan(-4, 0, 0)
                };
            } else {
                return new CropModelsRequest()
                {
                    Latitude = 10.477,
                    Longitude = -69.296,
                    Date = tomorrow,
                    UtcOffset = new TimeSpan(-4, 0, 0),
                    MinTemperature = 10,
                    MaxTemperature = 30,
                    OptimumTemperature = 20,
                    MinWetHours = 4,
                    MaxDryHours = 1
                };
            }
        }

        /// <summary>
        /// Builds a URL with an example query string for demonstration of GET request.
        /// </summary>
        private static string BuildUrlWithExampleQueryString(CropModelsRequest request) {
            string queryString = string.Empty;
            if (!string.IsNullOrWhiteSpace(request.ModelName)) {
                queryString = "/" + request.ModelName + "?latitude=" + request.Latitude + "&longitude=" + request.Longitude + "&date=" + request.Date + "&utcOffset=" + "-4:00";
            } else {
                queryString = "?latitude=" + request.Latitude +
                              "&longitude=" + request.Longitude +
                              "&date=" + request.Date +
                              "&utcOffset=" + request.UtcOffset +
                              "&minTemperature=" + request.MinTemperature +
                              "&maxTemperature=" + request.MaxTemperature +
                              "&optimumTemperature=" + request.OptimumTemperature +
                              "&minWetHours=" + request.MinWetHours +
                              "&maxDryHours=" + request.MaxDryHours;
            }

            return CROP_MODELS_URL + queryString;
        }

        #endregion Methods
    }
}