﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;


namespace aWhere.Api.Modeling.Sample {

    [DataContract]
    public class CropModelsResponse {

        [DataMember(Name = "requestId", Order = 1)]
        [DisplayName("requestId")]
        public int RequestId { get; set; }

        [DataMember(Name = "latitude", Order = 2)]
        [DisplayName("latitude")]
        public double Latitude { get; set; }

        [DataMember(Name = "longitude", Order = 3)]
        [DisplayName("longitude")]
        public double Longitude { get; set; }

        [DataMember(Name = "date", Order = 4)]
        [DisplayName("date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataMember(Name = "modelName", Order = 5)]
        [DisplayName("modelName")]
        public string ModelName { get; set; }

        [DataMember(Name = "modelIndexScore", Order = 6)]
        [DisplayName("modelIndexScore")]
        public double? ModelIndexScore { get; set; }

        [DataMember(Name = "modelClassification", Order = 7)]
        [DisplayName("modelClassification")]
        public string ModelClassification { get; set; }

        [DataMember(Name = "status", Order = 8)]
        [DisplayName("status")]
        public string Status { get; set; }
    }
}