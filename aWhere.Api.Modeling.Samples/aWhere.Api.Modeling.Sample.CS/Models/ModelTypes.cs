﻿namespace aWhere.Api.Modeling.Sample {

    public enum ModelTypes {
        CornGreyLeafSpot,
        CornRust,
        CornNorthernLeafBlight,
        CornAnthracnose,
        SoybeanRust,
        WheatandTriticaleLeafRust,
        WheatandTriticaleStripeRust
    }
}