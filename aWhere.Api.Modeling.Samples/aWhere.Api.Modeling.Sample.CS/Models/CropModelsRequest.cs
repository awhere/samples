﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace aWhere.Api.Modeling.Sample {

    [DataContract]
    public class CropModelsRequest {

        [DataMember(Name = "latitude")]
        [DisplayName("latitude")]
        public double Latitude { get; set; }

        [DataMember(Name = "longitude")]
        [DisplayName("longitude")]
        public double Longitude { get; set; }

        [DataMember(Name = "date")]
        [DisplayName("date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        [DataMember(Name = "utcOffset")]
        [DisplayName("utcOffset")]
        public TimeSpan UtcOffset { get; set; }

        [DataMember(Name = "modelName")]
        [DisplayName("modelName")]
        public string ModelName { get; set; }

        [DataMember(Name = "minTemperature")]
        [DisplayName("minTemperature")]
        public double? MinTemperature { get; set; }

        [DataMember(Name = "maxTemperature")]
        [DisplayName("maxTemperature")]
        public double? MaxTemperature { get; set; }

        [DataMember(Name = "optimumTemperature")]
        [DisplayName("optimumTemperature")]
        public double? OptimumTemperature { get; set; }

        [DataMember(Name = "minWetHours")]
        [DisplayName("minWetHours")]
        public short? MinWetHours { get; set; }

        [DataMember(Name = "maxDryHours")]
        [DisplayName("maxDryHours")]
        public short? MaxDryHours { get; set; }
    }
}