<?php

// Constants
$url = "https://data.awhere.com/api/models/crop";
$username = "username";
$password = "password";
$authKey = EncodeCredentials($username, $password);

// Usage Samples
$requests = BuildExampleCropModelsRequestsForPost();
print "Sample POST Response";
print_r(PostCropModelsRequest($requests, ""));
print "\n\n";

$requests = BuildExampleCropModelsRequestsForGet(false);
print "Sample GET Response:\n";
print_r(GetCropModels($requests));
print "\n\n";

$requests = BuildExampleCropModelsRequestsForGet(true);
print "Sample GET Response with model name specified:\n";	
print_r(GetCropModels($requests));
print "\n\n";


// Sample batch POST request to the Crop Models API, using the Web API 2.1 client library
//
// Returns an array of CropModelResponse objects either in json or PHP object form
function PostCropModelsRequest($cropModelRequests, $modelType = "", $json = true)
{
	global $url, $authKey;
	
	$cropUrl = empty($modelType) ? "$url/crop/" : "$url/crop/$modelType";
	
	$cropModelRequestsJSON = json_encode($cropModelRequests);
	
	$ch = curl_init($cropUrl);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $cropModelRequestsJSON);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
												"Content-Type: application/json",
												"Content-Length: ".strlen($cropModelRequestsJSON),
												"Accept: application/json",
												"Authorization: Basic $authKey"
											));
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	return $json ? CurlExecute($ch) : json_decode(CurlExecute($ch));
}

// Returns an array of CropModelResponse objects either in json or PHP object form
function GetCropModels($request, $json = true) 
{
	global $authKey;

	$ch = curl_init(BuildUrlWithExampleQueryString($request));
	
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
												"Content-Type: application/json",
												"Accept: application/json",
												"Authorization: Basic $authKey"
											));
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	return $json ? CurlExecute($ch) : json_decode(CurlExecute($ch));
}

// A utility function to encapsulate curl error handling
function CurlExecute($ch)
{
	$curlOutput = curl_exec($ch);
	if($curlOutput === false)
	{
		$curlOutput = "";
		die(curl_error($ch));
	}
	else
	{
		$info = curl_getinfo($ch);
		
		if($info['http_code'] != 200)
		{
			$curlOutput = "";
			die("HTTP Error: ".$info['http_code']." for ".$info['url']);
		}
	}	
	curl_close($ch);
	
	return $curlOutput;
}

/// Builds requests for demonstration of POST calls.
function BuildExampleCropModelsRequestsForPost() {
	$cropModelsRequests = array();

	$tomorrow = date("Y-m-d", strtotime("+1 day"));

	array_push($cropModelsRequests, array(
		"Latitude" => 41.672,
		"Longitude" => -91.347,
		"Date" => $tomorrow,
		"ModelName" => "CornRust",
		"UtcOffset" => "-00:06:00"
	));
	array_push($cropModelsRequests, array(
		"Latitude" => 59.213,
		"Longitude" => 25.621,
		"Date" => $tomorrow,
		"MinTemperature" => 10,
		"MaxTemperature" => 30,
		"OptimumTemperature" => 20,
		"MinWetHours" => 4,
		"MaxDryHours" => 1,
		"UtcOffset" => "00:02:00"
	));

	return $cropModelsRequests;
}

// Builds a request for demonstration of Get calls.
function BuildExampleCropModelsRequestsForGet($isModelNameSpecified)
{
	$cropModelsRequests = null;

	$tomorrow = strtotime("+1 day");

	if ($isModelNameSpecified) 
	{
		$cropModelsRequests = array(
			"ModelName" => "CornRust",
			"Latitude" => 10.477,
			"Longitude" => -69.296,
			"Date" => $tomorrow,
			"UtcOffset" => "-00:04:00"
		);
	}
	else 
	{
		$cropModelsRequests = array(
			"Latitude" => 10.477,
			"Longitude" => -69.296,
			"Date" => $tomorrow,
			"UtcOffset" => "-00:04:00",
			"MinTemperature" => 10,
			"MaxTemperature" => 30,
			"OptimumTemperature" => 20,
			"MinWetHours" => 4,
			"MaxDryHours" => 1
		);
	}
	
	return $cropModelsRequests;
}

// Builds a URL with an example query string for demonstration of GET request.
function BuildUrlWithExampleQueryString($request)
{
	global $url;
	$queryString = "";
	
	$dateString = date("Y-m-d", $request["Date"]);
	
	if (!empty($request["ModelName"])) 
	{
		$queryString .= "/".$request["ModelName"]."?latitude=".$request["Latitude"]."&longitude=".$request["Longitude"]."&date=$dateString&utcOffset=-4:00";
	}
	else 
	{
		$queryString = "?latitude=".$request["Latitude"].
					  "&longitude=".$request["Longitude"].
					  "&date=$dateString".
					  "&utcOffset=".$request["UtcOffset"].
					  "&minTemperature=".$request["MinTemperature"].
					  "&maxTemperature=".$request["MaxTemperature"].
					  "&optimumTemperature=".$request["OptimumTemperature"].
					  "&minWetHours=".$request["MinWetHours"].
					  "&maxDryHours=".$request["MaxDryHours"];
	}

	return $url.$queryString;
}
		
// Formats and encodes credentials appropriately for Basic Authentication
function EncodeCredentials($username, $password) 
{
	$encodedCredentials = base64_encode($username.":".$password);
	return $encodedCredentials;
}

?>
